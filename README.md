## rosemary-userdebug 11 RP1A.200720.011 320 test-keys
- Manufacturer: xiaomi
- Platform: mt6785
- Codename: rosemary
- Brand: Redmi
- Flavor: rosemary-userdebug
maltose-userdebug
rosemary-userdebug
secret-userdebug
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: 320
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Redmi/rosemary/rosemary:11/RP1A.200720.011/320:userdebug/test-keys
- OTA version: 
- Branch: rosemary-userdebug-11-RP1A.200720.011-320-test-keys
- Repo: redmi_rosemary_dump_19641


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
